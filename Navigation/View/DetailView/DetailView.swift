//
//  DetailView.swift
//  Navigation
//
//  Created by Hariharan S on 25/05/24.
//

import SwiftUI

struct DetailView: View {
    var number: Int
    @Binding var path: [Int]

    var body: some View {
        NavigationLink(
            "Go to Random Number",
            value: Int.random(in: 1...1000)
        )
        .navigationTitle("Number: \(number)")
        .toolbar {
            Button("Home") {
                path.removeAll()
            }
        }
    }
}

//#Preview {
//    DetailView()
//}
