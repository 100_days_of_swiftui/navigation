//
//  ContentView.swift
//  Navigation
//
//  Created by Hariharan S on 25/05/24.
//

import SwiftUI

struct ContentView: View {
    @State private var path = [Int]()
    
    var body: some View {
        NavigationStack(path: $path) {
                DetailView(
                    number: 0,
                    path: $path
                )
            .navigationDestination(for: Int.self) { i in
                DetailView(
                    number: i,
                    path: $path
                )
            }
        }
    }
}

#Preview {
    ContentView()
}
