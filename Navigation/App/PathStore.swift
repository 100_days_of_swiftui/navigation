//
//  PathStore.swift
//  Navigation
//
//  Created by Hariharan S on 25/05/24.
//

import Foundation
import Observation
import SwiftUI

@Observable
class PathStore {
    var path: NavigationPath {
        didSet {
            self.save()
        }
    }

    private let savePath = URL.documentsDirectory.appending(path: "SavedPath")

    init() {
        if let data = try? Data(contentsOf: savePath) {
            if let decoded = try? JSONDecoder().decode(
                NavigationPath.CodableRepresentation.self,
                from: data
            ) {
                self.path = NavigationPath(decoded)
                return
            }
        }
        self.path = NavigationPath()
    }

    func save() {
        guard let representation = self.path.codable
        else {
            return
        }
        do {
            let data = try JSONEncoder().encode(representation)
            try data.write(to: self.savePath)
        } catch {
            print("Failed to save navigation data")
        }
    }
}
