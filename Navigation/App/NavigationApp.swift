//
//  NavigationApp.swift
//  Navigation
//
//  Created by Hariharan S on 25/05/24.
//

import SwiftUI

@main
struct NavigationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
